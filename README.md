# recipe.app.api.proxy
# for testing commit3



NGINX proxy app  for our recipe app API

## usage

### Environment Variables

* 'LISTEN_PORT' - Port to listen to (default: '8000')
* 'APP_HOST' - Hostname of the app to forward requests to (default: 'app)
* 'APP_PORT' - port of the host (default: '9000') 